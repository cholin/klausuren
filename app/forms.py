# -*- coding: utf-8 -*-

from datetime import date
from flask import current_app
from flask.ext.wtf import Form
from wtforms import TextField, FileField, SelectField, validators
from wtforms.validators import ValidationError

year_start = date.today().year
year_end = current_app.config['FORM_START_YEAR']-1
choices = [(str(x),x) for x in  xrange(year_start, year_end, -1)]
class UploadForm(Form):
    """ Upload Form class for validation """
    study = TextField('Studiengang')
    exam   = FileField('Klausur')
    course = SelectField('Kurs')
    course_new = TextField('Modulname', validators=[validators.Optional(),
                                                    validators.Length(min=5)])
    year   = SelectField('Jahr', validators=[validators.Required()],
                         choices = choices)

    def validate_exam(form, field):
      exts = current_app.config['ALLOWED_EXTENSIONS']
      ext = map(field.data.filename.endswith, exts)
      if not any(ext):
        raise ValidationError(u'Ungültiger Dateityp')

      if field.data.content_length > current_app.config['MAX_CONTENT_LENGTH']:
        raise ValidationError(u'Zu große Datei')
    
    def validate_course(form, field):
      data = form.course.data
      if (data, data) not in field.choices or data == '':
        raise ValidationError(u'Bitte wähle einen Kurs!')


