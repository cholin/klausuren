# -*- coding: utf-8 -*-

from flask import Flask, render_template, g
from .main import main

def create_app(config=None):
    """Creates the Flask app."""
    app = Flask(__name__)

    configure_app(app)
    configure_error_handlers(app)

    app.register_blueprint(main)

    return app


def configure_app(app):
    app.config.from_pyfile('../config.cfg')

    # http://flask.pocoo.org/docs/0.10/errorhandling/
    if not app.debug:
        import logging
        from logging.handlers import RotatingFileHandler
        file_handler = RotatingFileHandler(app.config['LOG_FILE_PATH'])
        file_handler.setLevel(logging.WARNING)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s'
            '[in %(pathname)s:%(lineno)d]'))
        app.logger.addHandler(file_handler)


def configure_error_handlers(app):
    @app.route('/403')
    @app.errorhandler(403)
    def forbidden():
        return render_template('403.html'), 403

    @app.errorhandler(400)
    @app.errorhandler(404)
    def errorhandler(e):
        return render_template('404.html', error=e), e.code
