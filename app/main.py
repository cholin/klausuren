# -*- coding: utf-8 -*-

import os, sys
from flask import Blueprint, render_template, request, flash, redirect,\
                  url_for, current_app, g
from werkzeug import secure_filename
from wtforms.validators import ValidationError
from .backend import Storage


main = Blueprint('main', __name__)

def get_studies():
    """
      Add all existing courses of backend to study list.
      This list is used to fill form values with courses.
    """
    studies = getattr(g, '_studies', None)
    if studies is None:
      studies = g._studies = {}
      it = current_app.config['STUDIES'].items()
      for i, (abbr, courses) in enumerate(it):
        path_rel = os.path.join('studies', abbr)
        path_app = os.path.join('app', 'static', path_rel)
        studies[abbr] = Storage(path_rel, path_app)
    return studies


@main.route('/<study>/upload/', methods=['GET', 'POST'])
@main.route('/<study>/upload/<course>', methods=['GET', 'POST'])
def upload(study, course = None):
  from .forms import UploadForm
  form = UploadForm()
  form.study.data = study

  # dynamically fill form values
  courses = set(current_app.config['STUDIES'][study] +
                get_studies()[study].get_courses())
  choices = [(k,k) for k in sorted(courses, key=unicode.lower)]
  form.course.choices = choices
  if 'new' not in dict(form.course.choices):
    form.course.choices.append(('', u'---'))
    form.course.choices.append(('new', u'neuen Kurs hinzufügen'))

  if form.validate_on_submit():
      if form.course.data == 'new':
        course = form.course_new.data
      else:
        course = form.course.data

      year = form.year.data
      filename = secure_filename(form.exam.data.filename)
      try:
        data = form.exam.data.stream.getvalue()
      except:
        data = form.exam.data.stream.read()

      backend = get_studies()[study]
      if not backend.exam_exists(course, year, filename):
        backend.add_exam(course, year, filename, data)
        flash(u'Datei %s gespeichert.' % filename)
        return redirect(url_for('.courses_show', study = study, course = course))
      else:
        flash(u'Datei mit gleichem Namen existiert schon!', 'error')

  try:
    form.course.data = [k for (k,v) in form.course.choices if v == course][0]
  except:
    pass

  return render_template('upload.html', study = study, form = form,
                           course = course)


@main.route('/<study>/courses/')
@main.route('/<study>/courses/<course>')
def courses_show(study, course = None):
  """ Lists all courses or exams for a course """
  backend = get_studies()[study]
  if course:
    entries = sorted(backend.get_exams(course), reverse = True)
    return render_template('exams.html', study = study, course = course,
                             entries=entries)

  courses = sorted(backend.get_courses())
  return render_template('courses.html', study = study, courses = courses)


@main.route('/')
def index():
  """ Lists all course of studies """
  get_img_path = lambda x: os.path.join('studies', x, 'logo.png')
  it = current_app.config['STUDIES'].items()
  studies = [(name, get_img_path(name)) for name,m in it]

  return render_template('index.html', studies = studies)
